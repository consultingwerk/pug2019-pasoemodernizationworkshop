/*------------------------------------------------------------------------
    File        : dsCountry.i
    Purpose     : 

    Syntax      :

    Description : 

    Author(s)   : marko
    Created     : Wed Oct 16 14:32:57 CEST 2019
    Notes       :
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

   DEFINE TEMP-TABLE eCountry NO-UNDO 
        LIKE Country
        BEFORE-TABLE eCountryBefore
        .

    DEFINE DATASET dsCountry 
        FOR eCountry
        .