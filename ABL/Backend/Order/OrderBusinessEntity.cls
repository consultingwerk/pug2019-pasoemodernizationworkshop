/*------------------------------------------------------------------------
    File        : OrderBusinessEntity
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : marko
    Created     : Tue Oct 22 14:08:17 CEST 2019
    Notes       : 
  ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING Workshop.OERA.BusinessEntity.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS Backend.Order.OrderBusinessEntity 
    INHERITS BusinessEntity: 

    {Backend\Order\dsOrder.i}
    
    DEFINE DATA-SOURCE srcOrder FOR Order. 
        
    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    CONSTRUCTOR PUBLIC OrderBusinessEntity ():
        SUPER ().
        
        THIS-OBJECT:DatasetHandle = DATASET dsOrder:HANDLE.
        
    END CONSTRUCTOR.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    METHOD OVERRIDE PROTECTED VOID AttachDataSource ():
        
        BUFFER eOrder:ATTACH-DATA-SOURCE (DATA-SOURCE srcOrder:HANDLE).
        
    END.
        
    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    METHOD PUBLIC VOID CalculateTotals (INPUT-OUTPUT plcParameter AS LONGCHAR):
        
        DEFINE VARIABLE iNumOrders     AS INTEGER  NO-UNDO.
        DEFINE VARIABLE dAmountOrdered AS DECIMAL  NO-UNDO.
        DEFINE VARIABLE iCustNum       AS INTEGER  NO-UNDO.
        
        DEFINE BUFFER bOrder FOR Order.
        DEFINE BUFFER bOrderLine FOR OrderLine.
        
        iCustNum = INTEGER (ENTRY (1, plcParameter, CHR (1))).

        FOR EACH bOrder WHERE bOrder.custnum = iCustNum NO-LOCK:
            iNumOrders = iNumOrders + 1.
            
            FOR EACH bOrderLine OF bOrder NO-LOCK:
                dAmountOrdered = dAmountOrdered + ((bOrderLine.Price * bOrderLine.Qty) - ( ((bOrderLine.Discount / 100) * (bOrderLine.Price * bOrderLine.Qty)))). 
            END.
        END.
        
        plcParameter = SUBSTITUTE ("&2&1&3&1&4", CHR (1),
                                   iCustNum,
                                   iNumOrders,
                                   dAmountOrdered).

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    METHOD OVERRIDE PROTECTED VOID DetachDataSource ():
        
        BUFFER eOrder:DETACH-DATA-SOURCE ().
        
    END.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    METHOD OVERRIDE PROTECTED VOID ValidateData ():
        
        FOR EACH eOrder WHERE ROW-STATE (eOrder) = ROW-CREATED :
            
            IF eOrder.Ordernum = 0 OR eOrder.Ordernum = ? THEN 
                eOrder.Ordernum = NEXT-VALUE (NextOrdNum).
            
            MESSAGE eOrder.CustNum SKIP 
                    eOrder.Ordernum SKIP 
                    eorder.SalesRep
            VIEW-AS ALERT-BOX.
            
        END.
        
    END METHOD.

END CLASS.