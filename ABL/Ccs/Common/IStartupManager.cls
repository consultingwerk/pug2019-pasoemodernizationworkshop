/*------------------------------------------------------------------------
  This Software is licensed by Progress Software Corporation (licensor)
  under the Progress Software Common Component Specification Project
  Release License Agreement available at
  https://community.progress.com/products/directions/common_component/p/releaselicenseagreement

  The Interface definition is part of the Common Component Specification [CCSBE01]. The
  file is considered as a Specification Implementation Condition as described
  in section 2.1.1.1: If Licensor has made Specification Implementation
  Conditions available as of the date Licensee completes its Independent
  Implementation, then Licensee must, prior to making any claim that its
  Independent Implementation complies with the Specification, ensure that
  the Independent Implementation satisfies all of the Specification
  Implementation Conditions. If Licensor subsequently makes available or
  updates, from time to time, the Specification Implementation Conditions,
  then Licensee will verify that its Independent Implementation satisfies the
  latest version of the Specification Implementation Conditions within ninety
  (90) days following Licensor's release thereof.

  Contributors:
    Peter Judge, Progress Software Corp [2016]
  ----------------------------------------------------------------------*/
 /*------------------------------------------------------------------------
    File        : IStartupManager
    Purpose     : The factory of all common components.
    Syntax      :
    Description :
    Author(s)   : Simon L Prinsloo
    Created     : Wed May 18 19:41:55 CAT 2016
    Notes       :
  ----------------------------------------------------------------------*/

USING Ccs.Common.IManager FROM PROPATH.
USING Ccs.Common.IServiceManager FROM PROPATH.
USING Ccs.Common.ISessionManager FROM PROPATH.

INTERFACE Ccs.Common.IStartupManager INHERITS IManager:

  /*------------------------------------------------------------------------------
   Purpose: Provides access to the injected IServiceManager.
   Notes:
  ------------------------------------------------------------------------------*/
  DEFINE PUBLIC PROPERTY ServiceManager AS IServiceManager NO-UNDO GET.

  /*------------------------------------------------------------------------------
   Purpose: Provides access to the injected ISessionManager.
   Notes:
  ------------------------------------------------------------------------------*/
  DEFINE PUBLIC PROPERTY SessionManager AS ISessionManager NO-UNDO GET.

  /*------------------------------------------------------------------------------
  Purpose: Retrieve an instance of the specified IManager object.
  Notes:
  @param pServiceType The Progress.Lang.Class repersenting the required service.
  @return IManager implementation of the requested type, or ? if none are configured.
  ------------------------------------------------------------------------------*/
  METHOD PUBLIC IManager getManager ( pServiceType AS Progress.Lang.Class ).

END INTERFACE.
