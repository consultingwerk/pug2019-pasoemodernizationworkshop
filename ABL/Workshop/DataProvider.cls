/*------------------------------------------------------------------------
    File        : DataProvider
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : marko
    Created     : Thu Oct 17 15:50:30 CEST 2019
    Notes       : 
  ----------------------------------------------------------------------*/

BLOCK-LEVEL ON ERROR UNDO, THROW.

USING Progress.Lang.*.
USING Backend.Crm.* FROM PROPATH.
USING Workshop.OERA.* FROM PROPATH.
USING Ccs.BusinessLogic.* FROM PROPATH.
USING Workshop.* FROM PROPATH.

CLASS Workshop.DataProvider: 

    DEFINE PUBLIC PROPERTY DatasetHandle AS HANDLE NO-UNDO 
    GET.
    SET. 

    DEFINE VARIABLE hServer        AS HANDLE                      NO-UNDO.
    DEFINE VARIABLE hQuery         AS HANDLE                      NO-UNDO.
    DEFINE VARIABLE oBindingSource AS Progress.Data.BindingSource NO-UNDO.
        
    DEFINE VARIABLE cEntityName    AS CHARACTER                   NO-UNDO.
    DEFINE VARIABLE cTableName     AS CHARACTER                   NO-UNDO.
    DEFINE VARIABLE cQueryString   AS CHARACTER                   NO-UNDO.
    DEFINE VARIABLE cNextBatch     AS CHARACTER                   NO-UNDO.
    DEFINE VARIABLE cPrevBatch     AS CHARACTER                   NO-UNDO.
    
    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    CONSTRUCTOR PUBLIC DataProvider (phDataset AS HANDLE,
                                     oBindingSource AS Progress.Data.BindingSource):
        SUPER ().
        
        IF VALID-HANDLE (phDataset) THEN DO:
            THIS-OBJECT:DatasetHandle = phDataset.
        END.
            
        THIS-OBJECT:oBindingSource = oBindingSource.
        
        THIS-OBJECT:hServer = SessionProvider:GetSession ().
        
    END CONSTRUCTOR.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    METHOD PROTECTED VOID DisplayData ():
        
        THIS-OBJECT:hQuery:QUERY-OPEN ().
        
        IF VALID-OBJECT (THIS-OBJECT:oBindingSource) THEN 
            THIS-OBJECT:oBindingSource:RefreshAll ().
        
    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    METHOD PUBLIC VOID GetNextBatch ():
        
        DEFINE VARIABLE hDataset AS HANDLE NO-UNDO.
        
        RUN Backend/proSiGetData.p ON hServer (cEntityName,
                                               cTableName,
                                               cQueryString,
                                               cNextBatch,
                                               OUTPUT cNextBatch,
                                               OUTPUT cPrevBatch,
                                               OUTPUT DATASET-HANDLE hDataset).

        IF VALID-HANDLE (hDataset) THEN 
            DatasetHandle:COPY-DATASET (hDataset, FALSE, TRUE) .
        
        THIS-OBJECT:DisplayData ().

        FINALLY:
            IF VALID-HANDLE (hDataset) THEN DO:
                hDataset:EMPTY-DATASET ().
                DELETE OBJECT hDataset.
            END.
        END FINALLY.
        
    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    METHOD PUBLIC VOID InvokeMethod (pcEntityName AS CHARACTER,
                                     pcMethodName AS CHARACTER,
                                     INPUT-OUTPUT plcParameter AS LONGCHAR):
        
        RUN Backend/proSiInvokeMethod.p ON hServer (pcEntityName,
                                                    pcMethodName,
                                                    INPUT-OUTPUT plcParameter).

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    METHOD PUBLIC VOID RequestData (pcEntityName AS CHARACTER, 
                                    pcTableName AS CHARACTER,
                                    pcQueryString AS CHARACTER):

        DEFINE VARIABLE hDataset AS HANDLE NO-UNDO.

        ASSIGN 
            cEntityName  = pcEntityName
            cTableName   = pcTableName
            cQueryString = pcQueryString
            .

        RUN Backend/proSiGetData.p ON hServer (pcEntityName,
                                               pcTableName,
                                               pcQueryString,
                                               "",
                                               OUTPUT cNextBatch,
                                               OUTPUT cPrevBatch,
                                               OUTPUT DATASET-HANDLE hDataset).

        IF NOT VALID-HANDLE (DatasetHandle) THEN DO:
            
            CREATE DATASET DatasetHandle.
            
            DatasetHandle:CREATE-LIKE (hDataset).
            
        END.

        DatasetHandle:COPY-DATASET (hDataset, FALSE, TRUE) .

        IF NOT VALID-OBJECT (hQuery) THEN DO:
            
            CREATE QUERY hQuery.
        
            hQuery:ADD-BUFFER (DatasetHandle:GET-BUFFER-HANDLE (1)).
        
            hQuery:QUERY-PREPARE (SUBSTITUTE ("FOR EACH &1 INDEXED-REPOSITION":U, DatasetHandle:GET-BUFFER-HANDLE (1):NAME)).
        
            IF VALID-OBJECT (THIS-OBJECT:oBindingSource) THEN
                THIS-OBJECT:oBindingSource:Handle = THIS-OBJECT:hQuery.
        
        END.

        IF VALID-OBJECT (THIS-OBJECT:oBindingSource) THEN
            THIS-OBJECT:oBindingSource:Batching = TRUE.

        //DatasetHandle:WRITE-XMLSCHEMA ("file", "Backend\Crm\dsCustomer.xsd", TRUE).
        
        THIS-OBJECT:DisplayData ().
        
        THIS-OBJECT:SetTrackingChanges (TRUE).
        
    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    METHOD PUBLIC VOID SaveChanges ():

        DEFINE VARIABLE hChangeDataset AS HANDLE NO-UNDO.
                
        THIS-OBJECT:SetTrackingChanges (FALSE).
        
        CREATE DATASET hChangeDataset.
        hChangeDataset:CREATE-LIKE (THIS-OBJECT:DatasetHandle).
        hChangeDataset:GET-CHANGES (THIS-OBJECT:DatasetHandle).
        
        hChangeDataset:GET-BUFFER-HANDLE (1):FIND-FIRST ().

        RUN Backend/proSiSubmit.p ON hServer (cEntityName,
                                              cTableName,
                                              INPUT-OUTPUT DATASET-HANDLE hChangeDataset).
        
        IF hChangeDataset:ERROR THEN DO:
            MESSAGE "Error while saving changes occured!"
            VIEW-AS ALERT-BOX.
            
            RETURN.
        END.
        
        hChangeDataset:MERGE-CHANGES (THIS-OBJECT:DatasetHandle).

        IF VALID-OBJECT (THIS-OBJECT:oBindingSource) THEN
            THIS-OBJECT:oBindingSource:Refresh ().
        
        FINALLY:
            hChangeDataset:EMPTY-DATASET ().
            DELETE OBJECT hChangeDataset.
            
            THIS-OBJECT:SetTrackingChanges (TRUE).
        END FINALLY.

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    METHOD PROTECTED VOID SetTrackingChanges (plTrackingChanges AS LOGICAL):
        
        DEFINE VARIABLE iBuffer AS INTEGER NO-UNDO.
        DEFINE VARIABLE hBuffer AS HANDLE  NO-UNDO.
        
        DO iBuffer = 1 TO THIS-OBJECT:DatasetHandle:NUM-BUFFERS :

            hBuffer = THIS-OBJECT:DatasetHandle:GET-BUFFER-HANDLE (iBuffer).
            
            IF VALID-HANDLE (hBuffer:TABLE-HANDLE:BEFORE-TABLE) THEN DO: 
                hBuffer:TABLE-HANDLE:TRACKING-CHANGES = plTrackingChanges.
            END.
        END.

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    METHOD PUBLIC VOID UndoChanges ():
        
        THIS-OBJECT:SetTrackingChanges (FALSE).
        
        THIS-OBJECT:DatasetHandle:REJECT-CHANGES ().
        
        IF VALID-OBJECT (THIS-OBJECT:oBindingSource) THEN
            THIS-OBJECT:oBindingSource:Refresh ().
        
        FINALLY:
            THIS-OBJECT:SetTrackingChanges (TRUE).
        END FINALLY.
        
    END METHOD.

END CLASS.