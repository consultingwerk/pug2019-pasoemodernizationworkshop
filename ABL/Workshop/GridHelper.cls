 
 /*------------------------------------------------------------------------
    File        : GridHelper
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : marko
    Created     : Fri Oct 18 16:56:41 CEST 2019
    Notes       : 
  ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING Infragistics.Win.UltraWinGrid.* FROM ASSEMBLY.
USING Infragistics.Win.* FROM ASSEMBLY.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS Workshop.GridHelper: 

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    METHOD PUBLIC STATIC VOID SetGridReadOnly (poGrid AS UltraGrid):
        
        DEFINE VARIABLE i     AS INTEGER       NO-UNDO.
        DEFINE VARIABLE j     AS INTEGER       NO-UNDO.
        DEFINE VARIABLE oBand AS UltraGridBand NO-UNDO.

        poGrid:DisplayLayout:Override:AllowAddNew = AllowAddNew:No.
        poGrid:DisplayLayout:Override:AllowDelete = DefaultableBoolean:False.
        poGrid:DisplayLayout:Override:AllowUpdate = DefaultableBoolean:False.

        poGrid:DisplayLayout:Override:SelectTypeRow = SelectType:SingleAutoDrag .

        DO i = 1 TO poGrid:DisplayLayout:Bands:Count :
            oBand = poGrid:DisplayLayout:Bands[i - 1].

            IF Progress.Util.EnumHelper:AreEqual(oBand:Override:SelectTypeCell, SelectType:Default) THEN
                oBand:Override:SelectTypeCell = SelectType:None.
            IF Progress.Util.EnumHelper:AreEqual(oBand:Override:SelectTypeCol, SelectType:Default) THEN
                oBand:Override:SelectTypeCol  = SelectType:None.
            IF Progress.Util.EnumHelper:AreEqual(oBand:Override:SelectTypeRow, SelectType:Default) THEN
                oBand:Override:SelectTypeRow  = SelectType:SingleAutoDrag.
            IF Progress.Util.EnumHelper:AreEqual(oBand:Override:SelectTypeRow, SelectType:Single) THEN
                oBand:Override:SelectTypeRow  = SelectType:SingleAutoDrag.

            DO j = 1 TO oBand:Columns:Count :
                oBand:Columns[j - 1]:CellActivation = Activation:NoEdit .
            END.
        END.

    END METHOD.

END CLASS.