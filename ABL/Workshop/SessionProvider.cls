/*------------------------------------------------------------------------
    File        : SessionProvider
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : marko
    Created     : Tue Oct 22 10:34:18 CEST 2019
    Notes       : 
  ----------------------------------------------------------------------*/

USING Progress.Lang.*.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS Workshop.SessionProvider: 

    DEFINE PRIVATE STATIC PROPERTY AppserverUrl AS CHARACTER NO-UNDO INITIAL "http://localhost:10000/apsv"
    GET.
    SET. 

    DEFINE PRIVATE STATIC PROPERTY SessionHandle AS HANDLE NO-UNDO 
    GET.
    SET. 

    DEFINE PUBLIC STATIC PROPERTY UseAppserverConnection AS LOGICAL NO-UNDO 
    GET.
    SET. 

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    METHOD PUBLIC STATIC HANDLE GetSession ():
        
        IF UseAppserverConnection THEN DO:
            IF NOT VALID-HANDLE (SessionHandle) THEN DO:
                CREATE SERVER SessionHandle.
                SessionHandle:CONNECT (SUBSTITUTE ("-URL &1", AppserverUrl)).
            END.
        END.
        ELSE         
            SessionHandle = SESSION.
        
        RETURN SessionHandle.

    END METHOD.

END CLASS.