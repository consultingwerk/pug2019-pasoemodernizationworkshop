@echo off
rem Replacement of the startup.bat script shipped by Tomcat
rem PAS for OpenEdge requires startup by tcman so that the property
rem a %CATALINA_PID% file is created.

call tcman.bat start

exit /b %errorlevel%

