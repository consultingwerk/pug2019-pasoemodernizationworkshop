&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DECLARATIONS Procedure
USING Workshop.* FROM PROPATH.
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
DEFINE INPUT  PARAMETER pcTable         AS CHARACTER    NO-UNDO.
DEFINE INPUT  PARAMETER pcDescription   AS CHARACTER    NO-UNDO.
DEFINE INPUT  PARAMETER pcResultField   AS CHARACTER    NO-UNDO.
DEFINE INPUT  PARAMETER pcDescrField    AS CHARACTER    NO-UNDO.
DEFINE OUTPUT PARAMETER poResult        AS CHARACTER    NO-UNDO.
DEFINE OUTPUT PARAMETER poDescr         AS CHARACTER    NO-UNDO.

DEFINE VARIABLE hQuery                  AS HANDLE       NO-UNDO.
DEFINE VARIABLE hBuffer                 AS HANDLE       NO-UNDO.
DEFINE VARIABLE cQuery                  AS CHARACTER    NO-UNDO.
DEFINE VARIABLE hBrowse                 AS HANDLE       NO-UNDO.
DEFINE VARIABLE cSearchFields           AS CHARACTER    NO-UNDO.

DEFINE VARIABLE oGenericDataProvider    AS DataProvider NO-UNDO.
DEFINE VARIABLE hDataset                AS HANDLE       NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS browseplaceholder FILL-IN-1 Btn_Cancel ~
Btn_OK 
&Scoped-Define DISPLAYED-OBJECTS FILL-IN-1 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "OK" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE FILL-IN-1 AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN 
     SIZE 154 BY 1 NO-UNDO.

DEFINE RECTANGLE browseplaceholder
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 162 BY 23.81.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     FILL-IN-1 AT ROW 1.48 COL 8 COLON-ALIGNED WIDGET-ID 4
     Btn_Cancel AT ROW 27.43 COL 133
     Btn_OK AT ROW 27.43 COL 149
     browseplaceholder AT ROW 2.91 COL 2 WIDGET-ID 2
     SPACE(1.59) SKIP(2.70)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "<insert dialog title>"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* <insert dialog title> */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* OK */
DO:
  
  IF hBuffer:AVAILABLE THEN
  DO:
    poResult = hBuffer:BUFFER-FIELD(pcResultField):BUFFER-VALUE.  
    IF pcDescrField <> ? AND pcDescrField <> "" THEN poDescr  = hBuffer:BUFFER-FIELD(pcDescrField):BUFFER-VALUE.
  END.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME FILL-IN-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL FILL-IN-1 Dialog-Frame
ON VALUE-CHANGED OF FILL-IN-1 IN FRAME Dialog-Frame /* Search */
DO:
  DEFINE VARIABLE i AS INTEGER     NO-UNDO.
  DEFINE VARIABLE cWhere AS CHARACTER   NO-UNDO.

  
   
   
   cWhere = "WHERE ".
   DO i = 1 TO NUM-ENTRIES(cSearchFields):
       IF TRIM(ENTRY(i, cSearchFields)) > "" THEN
       DO:
           cWhere = SUBSTITUTE ("&1 &2 MATCHES '&3'",cWhere, ENTRY(i, cSearchFields),"*" + SELF:SCREEN-VALUE + "*").
           IF i <> NUM-ENTRIES(cSearchFields) THEN cWhere = cWhere + " OR ".
       END.
   END.
   
   hDataset:EMPTY-DATASET ().
    
   oGenericDataProvider:RequestData ("Backend.Generic.GenericBusinessEntity",
                                      pcTable, 
                                      cWhere).
    
   hDataset = oGenericDataProvider:DatasetHandle.
    
   hBuffer = hDataset:GET-BUFFER-HANDLE (1).
   cQuery = SUBSTITUTE ("FOR EACH &1", hBuffer:NAME).
       
   IF VALID-HANDLE(hQuery) THEN hQuery:QUERY-CLOSE().
    
   hQuery:QUERY-PREPARE(cQuery).
   hQuery:QUERY-OPEN().
  
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT EQ ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.


CREATE QUERY hQuery.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
    browseplaceholder:HIDDEN  = TRUE.
DEFINE VARIABLE i AS INTEGER     NO-UNDO.

IF VALID-HANDLE(hBrowse) THEN DELETE OBJECT hBrowse.



pcTable = TRIM (pcTable).
oGenericDataProvider = NEW DataProvider (hDataset, ?).
oGenericDataProvider:RequestData ("Backend.Generic.GenericBusinessEntity",
                                  pcTable, 
                                  "").

hDataset = oGenericDataProvider:DatasetHandle.

hBuffer = hDataset:GET-BUFFER-HANDLE (1).
cQuery = SUBSTITUTE ("FOR EACH &1", hBuffer:NAME).

hQuery:SET-BUFFERS(hBuffer).
hQuery:QUERY-PREPARE(cQuery).
hQuery:QUERY-OPEN().
hQuery:GET-FIRST(NO-LOCK).

FRAME {&FRAME-NAME}:TITLE     = SUBSTITUTE("Search for: &1", pcDescription) .

    CREATE BROWSE hBrowse
      ASSIGN 
        FRAME     = FRAME {&FRAME-NAME}:HANDLE
        QUERY     = hQuery
        X         = browseplaceholder:X
        Y         = browseplaceholder:Y
        WIDTH     = browseplaceholder:WIDTH
        HEIGHT    = browseplaceholder:HEIGHT
        VISIBLE   = YES
        READ-ONLY = NO
        SENSITIVE = TRUE.
ON MOUSE-SELECT-DBLCLICK OF hBrowse
DO:
APPLY "choose" TO btn_OK IN FRAME {&FRAME-NAME}.
END.

DEFINE VARIABLE lResultFieldFound AS LOGICAL     NO-UNDO.


cSearchFields = "".
lResultFieldFound = FALSE.
DO i = 1 TO hBuffer:NUM-FIELDS :
    hBrowse:ADD-LIKE-COLUMN( SUBSTITUTE("&1.&2", hBuffer:NAME, hBuffer:BUFFER-FIELD(i):NAME)) NO-ERROR.  
    IF hBuffer:BUFFER-FIELD(i):DATA-TYPE = "character" THEN
    DO:
        cSearchFields = SUBSTITUTE("&1,&2", cSearchFields, hBuffer:BUFFER-FIELD(i):NAME).     
    END.
    IF hBuffer:BUFFER-FIELD(i):NAME = pcResultField THEN lResultFieldFound = TRUE.
END.      
  
  IF lResultFieldFound = FALSE THEN
  DO:
     MESSAGE "You must supply a result field"
         VIEW-AS ALERT-BOX INFORMATION BUTTONS OK.
     APPLY "choose" TO btn_cancel IN FRAME {&FRAME-NAME}.    
  END.


  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY FILL-IN-1 
      WITH FRAME Dialog-Frame.
  ENABLE browseplaceholder FILL-IN-1 Btn_Cancel Btn_OK 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

