&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          sports2000       PROGRESS
*/
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

{Backend/crm/dsCrm.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME BROWSE-2

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES Customer Salesrep

/* Definitions for BROWSE BROWSE-2                                      */
&Scoped-define FIELDS-IN-QUERY-BROWSE-2 Customer.Name Customer.Contact ~
Customer.City Customer.Country 
&Scoped-define ENABLED-FIELDS-IN-QUERY-BROWSE-2 
&Scoped-define QUERY-STRING-BROWSE-2 FOR EACH Customer NO-LOCK INDEXED-REPOSITION
&Scoped-define OPEN-QUERY-BROWSE-2 OPEN QUERY BROWSE-2 FOR EACH Customer NO-LOCK INDEXED-REPOSITION.
&Scoped-define TABLES-IN-QUERY-BROWSE-2 Customer
&Scoped-define FIRST-TABLE-IN-QUERY-BROWSE-2 Customer


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-BROWSE-2}

/* Definitions for FRAME fDetail                                        */
&Scoped-define FIELDS-IN-QUERY-fDetail Customer.Name Customer.CustNum ~
Customer.Address Customer.Address2 Customer.City Customer.PostalCode ~
Customer.State Customer.Country Customer.Contact Customer.EmailAddress ~
Customer.Phone Customer.Fax Salesrep.RepName Customer.SalesRep ~
Customer.CreditLimit Customer.Discount Customer.Balance Customer.Terms ~
Customer.Comments 
&Scoped-define ENABLED-FIELDS-IN-QUERY-fDetail Customer.Name ~
Customer.CustNum Customer.Address Customer.Address2 Customer.City ~
Customer.PostalCode Customer.State Customer.Country Customer.Contact ~
Customer.EmailAddress Customer.Phone Customer.Fax Salesrep.RepName ~
Customer.SalesRep Customer.CreditLimit Customer.Discount Customer.Balance ~
Customer.Terms Customer.Comments 
&Scoped-define ENABLED-TABLES-IN-QUERY-fDetail Customer Salesrep
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-fDetail Customer
&Scoped-define SECOND-ENABLED-TABLE-IN-QUERY-fDetail Salesrep
&Scoped-define QUERY-STRING-fDetail FOR EACH Customer SHARE-LOCK, ~
      EACH Salesrep OF Customer SHARE-LOCK
&Scoped-define OPEN-QUERY-fDetail OPEN QUERY fDetail FOR EACH Customer SHARE-LOCK, ~
      EACH Salesrep OF Customer SHARE-LOCK.
&Scoped-define TABLES-IN-QUERY-fDetail Customer Salesrep
&Scoped-define FIRST-TABLE-IN-QUERY-fDetail Customer
&Scoped-define SECOND-TABLE-IN-QUERY-fDetail Salesrep


/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS btnCancel RECT-1 btnSearch btnSave btnAdd ~
btnEdit btnDelete btnOrders BROWSE-2 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON btnAdd 
     IMAGE-UP FILE "C:/PUG2019/src/crm/icons/add.ico":U
     LABEL "Button 3" 
     SIZE 8 BY 1.91 TOOLTIP "Add Customer".

DEFINE BUTTON btnCancel 
     IMAGE-UP FILE "C:/PUG2019/src/crm/icons/sign_stop.ico":U
     LABEL "Button 7" 
     SIZE 8 BY 1.91 TOOLTIP "Cancel".

DEFINE BUTTON btnDelete 
     IMAGE-UP FILE "C:/PUG2019/src/crm/icons/delete.ico":U
     LABEL "Button 5" 
     SIZE 8 BY 1.91 TOOLTIP "Delete Customer".

DEFINE BUTTON btnEdit 
     IMAGE-UP FILE "C:/PUG2019/src/crm/icons/edit.ico":U
     LABEL "Button 4" 
     SIZE 8 BY 1.91 TOOLTIP "Edit Customer".

DEFINE BUTTON btnOrders 
     IMAGE-UP FILE "C:/PUG2019/src/crm/icons/purchase_order_cart.ico":U
     LABEL "Button 6" 
     SIZE 8 BY 1.91 TOOLTIP "Orders".

DEFINE BUTTON btnSave 
     IMAGE-UP FILE "C:/PUG2019/src/crm/icons/data_floppy_disk.ico":U
     LABEL "" 
     SIZE 8 BY 1.91 TOOLTIP "Save".

DEFINE BUTTON btnSearch 
     IMAGE-UP FILE "C:/PUG2019/src/crm/icons/magnifying_glass.ico":U
     LABEL "Button 1" 
     SIZE 8 BY 1.91 TOOLTIP "Find Customer".

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 250 BY .05.

DEFINE BUTTON BUTTON-1 
     IMAGE-UP FILE "sr/magnifying_glass.jpg":U NO-FOCUS FLAT-BUTTON NO-CONVERT-3D-COLORS
     LABEL "Button 1" 
     SIZE 4.6 BY 1.1.

DEFINE BUTTON BUTTON-2 
     IMAGE-UP FILE "sr/magnifying_glass.jpg":U NO-FOCUS FLAT-BUTTON NO-CONVERT-3D-COLORS
     LABEL "Button 2" 
     SIZE 4.6 BY 1.1.

DEFINE BUTTON BUTTON-3 
     IMAGE-UP FILE "sr/magnifying_glass.jpg":U NO-FOCUS FLAT-BUTTON NO-CONVERT-3D-COLORS
     LABEL "Button 3" 
     SIZE 4.6 BY 1.1.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 117 BY 4.57.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 117 BY 5.05.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 117 BY 2.38.

DEFINE RECTANGLE RECT-5
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 117 BY 5.38.

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 117 BY 7.71.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY BROWSE-2 FOR 
      Customer SCROLLING.

DEFINE QUERY fDetail FOR 
      Customer, 
      Salesrep SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE BROWSE-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS BROWSE-2 C-Win _STRUCTURED
  QUERY BROWSE-2 NO-LOCK DISPLAY
      Customer.Name FORMAT "x(30)":U
      Customer.Contact FORMAT "x(30)":U
      Customer.City FORMAT "x(25)":U
      Customer.Country FORMAT "x(20)":U
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 130 BY 28.81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     btnCancel AT ROW 1 COL 1 WIDGET-ID 16
     btnSearch AT ROW 1 COL 2 WIDGET-ID 2
     btnSave AT ROW 1 COL 9 WIDGET-ID 18
     btnAdd AT ROW 1 COL 11 WIDGET-ID 6
     btnEdit AT ROW 1 COL 20 WIDGET-ID 8
     btnDelete AT ROW 1 COL 29.2 WIDGET-ID 10
     btnOrders AT ROW 1 COL 47 WIDGET-ID 12
     BROWSE-2 AT ROW 3.14 COL 1 WIDGET-ID 200
     RECT-1 AT ROW 2.95 COL 1 WIDGET-ID 14
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 250.6 BY 31.05 WIDGET-ID 100.

DEFINE FRAME fDetail
     BUTTON-1 AT ROW 14.57 COL 20.4 WIDGET-ID 56
     Customer.Name AT ROW 1.67 COL 9 COLON-ALIGNED WIDGET-ID 2
          VIEW-AS FILL-IN 
          SIZE 106 BY 1
     Customer.CustNum AT ROW 2.91 COL 9.2 COLON-ALIGNED WIDGET-ID 54
          LABEL "Number" FORMAT "zzz,zzz,zz9"
          VIEW-AS FILL-IN 
          SIZE 23.8 BY 1
     Customer.Address AT ROW 5.1 COL 9 COLON-ALIGNED WIDGET-ID 4
          LABEL "Street"
          VIEW-AS FILL-IN 
          SIZE 106 BY 1
     Customer.Address2 AT ROW 6.05 COL 9 COLON-ALIGNED NO-LABEL WIDGET-ID 6
          VIEW-AS FILL-IN 
          SIZE 106 BY 1
     Customer.City AT ROW 7.38 COL 9 COLON-ALIGNED WIDGET-ID 8
          VIEW-AS FILL-IN 
          SIZE 19 BY 1
     Customer.PostalCode AT ROW 7.38 COL 41.4 COLON-ALIGNED WIDGET-ID 34
          VIEW-AS FILL-IN 
          SIZE 15.6 BY 1
     Customer.State AT ROW 7.38 COL 63.6 COLON-ALIGNED WIDGET-ID 10
          VIEW-AS FILL-IN 
          SIZE 16.4 BY 1
     Customer.Country AT ROW 7.38 COL 92.2 COLON-ALIGNED WIDGET-ID 12
          VIEW-AS FILL-IN 
          SIZE 18.8 BY 1
     Customer.Contact AT ROW 9.86 COL 9 COLON-ALIGNED WIDGET-ID 20
          VIEW-AS FILL-IN 
          SIZE 106 BY 1
     Customer.EmailAddress AT ROW 11.05 COL 9 COLON-ALIGNED WIDGET-ID 28
          VIEW-AS FILL-IN 
          SIZE 106 BY 1
     BUTTON-2 AT ROW 7.33 COL 112.4 WIDGET-ID 58
     Customer.Phone AT ROW 12.24 COL 9 COLON-ALIGNED WIDGET-ID 32
          VIEW-AS FILL-IN 
          SIZE 34 BY 1
     Customer.Fax AT ROW 12.24 COL 79 COLON-ALIGNED WIDGET-ID 30
          VIEW-AS FILL-IN 
          SIZE 36 BY 1
     Salesrep.RepName AT ROW 14.57 COL 44.8 COLON-ALIGNED WIDGET-ID 42
          LABEL "Name"
          VIEW-AS FILL-IN 
          SIZE 70 BY 1
     Customer.SalesRep AT ROW 14.62 COL 9 COLON-ALIGNED WIDGET-ID 36
          LABEL "Code"
          VIEW-AS FILL-IN 
          SIZE 9.6 BY 1
     Customer.CreditLimit AT ROW 17.29 COL 19 COLON-ALIGNED WIDGET-ID 22
          VIEW-AS FILL-IN 
          SIZE 29 BY 1
     Customer.Discount AT ROW 17.29 COL 69.2 COLON-ALIGNED WIDGET-ID 26
          VIEW-AS FILL-IN 
          SIZE 11 BY 1
     BUTTON-3 AT ROW 7.33 COL 80.8 WIDGET-ID 66
     Customer.Balance AT ROW 18.52 COL 19 COLON-ALIGNED WIDGET-ID 16
          VIEW-AS FILL-IN 
          SIZE 29 BY 1
     Customer.Terms AT ROW 18.52 COL 69.2 COLON-ALIGNED WIDGET-ID 38
          LABEL "Payment Terms"
          VIEW-AS FILL-IN 
          SIZE 22 BY 1
     Customer.Comments AT ROW 22.43 COL 1 NO-LABEL WIDGET-ID 52
          VIEW-AS EDITOR SCROLLBAR-VERTICAL
          SIZE 117 BY 6.38
     " Comments" VIEW-AS TEXT
          SIZE 12 BY .62 AT ROW 21.33 COL 2 WIDGET-ID 64
     " Address" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 4.24 COL 2.2 WIDGET-ID 40
     " Contact" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 8.71 COL 2.2 WIDGET-ID 46
     " Sales Representative" VIEW-AS TEXT
          SIZE 22 BY .62 AT ROW 13.67 COL 2 WIDGET-ID 48
     RECT-2 AT ROW 4.62 COL 1 WIDGET-ID 14
     RECT-3 AT ROW 9.1 COL 1 WIDGET-ID 44
     RECT-4 AT ROW 14.05 COL 1 WIDGET-ID 50
     RECT-5 AT ROW 16.33 COL 1 WIDGET-ID 60
     RECT-6 AT ROW 21.62 COL 1 WIDGET-ID 62
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 133 ROW 3.14
         SIZE 118 BY 28.33 WIDGET-ID 300.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Customer Maintenance"
         HEIGHT             = 31.05
         WIDTH              = 252
         MAX-HEIGHT         = 35.62
         MAX-WIDTH          = 252.8
         VIRTUAL-HEIGHT     = 35.62
         VIRTUAL-WIDTH      = 252.8
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

&IF '{&WINDOW-SYSTEM}' NE 'TTY' &THEN
IF NOT C-Win:LOAD-ICON("C:/PUG2019/src/crm/icons/dude.ico":U) THEN
    MESSAGE "Unable to load icon: C:/PUG2019/src/crm/icons/dude.ico"
            VIEW-AS ALERT-BOX WARNING BUTTONS OK.
&ENDIF
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME fDetail:FRAME = FRAME DEFAULT-FRAME:HANDLE.

/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* BROWSE-TAB BROWSE-2 btnOrders DEFAULT-FRAME */
/* SETTINGS FOR FRAME fDetail
                                                                        */
/* SETTINGS FOR FILL-IN Customer.Address IN FRAME fDetail
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Customer.CustNum IN FRAME fDetail
   EXP-LABEL EXP-FORMAT                                                 */
ASSIGN 
       Customer.CustNum:READ-ONLY IN FRAME fDetail        = TRUE.

/* SETTINGS FOR FILL-IN Salesrep.RepName IN FRAME fDetail
   EXP-LABEL                                                            */
ASSIGN 
       Salesrep.RepName:READ-ONLY IN FRAME fDetail        = TRUE.

/* SETTINGS FOR FILL-IN Customer.SalesRep IN FRAME fDetail
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN Customer.Terms IN FRAME fDetail
   EXP-LABEL                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE BROWSE-2
/* Query rebuild information for BROWSE BROWSE-2
     _TblList          = "sports2000.Customer"
     _Options          = "NO-LOCK INDEXED-REPOSITION"
     _FldNameList[1]   = sports2000.Customer.Name
     _FldNameList[2]   = sports2000.Customer.Contact
     _FldNameList[3]   = sports2000.Customer.City
     _FldNameList[4]   = sports2000.Customer.Country
     _Query            is OPENED
*/  /* BROWSE BROWSE-2 */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fDetail
/* Query rebuild information for FRAME fDetail
     _TblList          = "sports2000.Customer,sports2000.Salesrep OF sports2000.Customer"
     _Query            is OPENED
*/  /* FRAME fDetail */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Customer Maintenance */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Customer Maintenance */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME BROWSE-2
&Scoped-define SELF-NAME BROWSE-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BROWSE-2 C-Win
ON OFF-END OF BROWSE-2 IN FRAME DEFAULT-FRAME
DO:
            MESSAGE 1
                VIEW-AS ALERT-BOX INFORMATION BUTTONS OK.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BROWSE-2 C-Win
ON VALUE-CHANGED OF BROWSE-2 IN FRAME DEFAULT-FRAME
DO:
   IF AVAILABLE customer THEN
  DO:
     DISPLAY customer WITH FRAME fDetail.
     
  END. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btnAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btnAdd C-Win
ON CHOOSE OF btnAdd IN FRAME DEFAULT-FRAME /* Button 3 */
DO:
  RUN ipAdd.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btnCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btnCancel C-Win
ON CHOOSE OF btnCancel IN FRAME DEFAULT-FRAME /* Button 7 */
DO:
  undoblock: 
  DO:
      RUN ipSetButtonState(FALSE).
      DISABLE ALL WITH FRAME fDetail.
      UNDO, NEXT.
  END.
  
  MESSAGE AVAIL customer
      VIEW-AS ALERT-BOX INFORMATION BUTTONS OK.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btnDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btnDelete C-Win
ON CHOOSE OF btnDelete IN FRAME DEFAULT-FRAME /* Button 5 */
DO:
  RUN ipDelete.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btnEdit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btnEdit C-Win
ON CHOOSE OF btnEdit IN FRAME DEFAULT-FRAME /* Button 4 */
DO:
  IF AVAILABLE customer THEN
  DO:
    RUN ipSetButtonState(TRUE).
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btnOrders
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btnOrders C-Win
ON CHOOSE OF btnOrders IN FRAME DEFAULT-FRAME /* Button 6 */
DO:
  IF AVAILABLE customer THEN RUN order/wOrder.w (INPUT customer.custnum).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btnSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btnSave C-Win
ON CHOOSE OF btnSave IN FRAME DEFAULT-FRAME
DO: 
  DEFINE VARIABLE rCustomer AS ROWID NO-UNDO.
  
  RUN ipSetButtonState(FALSE).
  DO TRANSACTION:
      FIND CURRENT customer EXCLUSIVE-LOCK NO-ERROR NO-WAIT.
      ASSIGN INPUT FRAME fDetail
        Address
        Address2
        Balance
        City
        Comments
        Contact
        Country
        CreditLimit
        CustNum
        Discount
        EmailAddress
        Fax
        Name
        Phone
        PostalCode
        SalesRep
        State
        Terms.
      
  END.
  rCustomer = ROWID(customer).
  CLOSE QUERY browse-2.
  OPEN QUERY BROWSE-2 FOR EACH customer.
  QUERY browse-2:REPOSITION-TO-ROWID(rCustomer). 

  
  
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btnSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btnSearch C-Win
ON CHOOSE OF btnSearch IN FRAME DEFAULT-FRAME /* Button 1 */
DO:
  DEFINE VARIABLE cCustNum AS CHARACTER   NO-UNDO.
  DEFINE VARIABLE cDummy AS CHARACTER   NO-UNDO.
  
  DEFINE BUFFER bCustomer FOR Customer.
  
  RUN sr/wSearch.w ("Customer","Customers", "CustNum",?, OUTPUT cCustNum, OUTPUT cDummy).
  
  IF cCustNum <> ? AND cCustnum <> "" THEN
  DO:
     FIND bCustomer WHERE bCustomer.custnum = INTEGER(cCustNum) NO-LOCK NO-ERROR.
     IF AVAILABLE bCustomer THEN
     DO:
        QUERY browse-2:REPOSITION-TO-ROWID(ROWID(bCustomer)). 
        APPLY "value-changed" TO BROWSE-2. 
     END.
  END.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fDetail
&Scoped-define SELF-NAME BUTTON-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-1 C-Win
ON CHOOSE OF BUTTON-1 IN FRAME fDetail /* Button 1 */
DO:
    DEFINE VARIABLE cSalesRep AS CHARACTER   NO-UNDO.
    DEFINE VARIABLE cRepName AS CHARACTER   NO-UNDO.
  RUN sr/wSearch.w ("SalesRep","Sales Representatives", "salesrep","repname", OUTPUT cSalesrep, OUTPUT cRepName).
  
  IF cSalesRep <> "" AND cSalesRep <> ? THEN
  DO:
     DISPLAY 
         cSalesRep @ customer.Salesrep
         cRepname  @ sales.Repname
         WITH FRAME fDetail.
  
     APPLY "value-changed" TO customer.Salesrep IN FRAME fDetail.
  END.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-2 C-Win
ON CHOOSE OF BUTTON-2 IN FRAME fDetail /* Button 2 */
DO:
    DEFINE VARIABLE cCountry AS CHARACTER   NO-UNDO.
    DEFINE VARIABLE cDummy AS CHARACTER   NO-UNDO.
  RUN sr/wSearch.w ("Country","Country", "country",?, OUTPUT cCountry, OUTPUT cDummy).
  
  IF cCountry <> "" AND cCountry <> ? THEN
  DO:
     DISPLAY 
         cCountry @ customer.Country
         WITH FRAME fDetail.
         
         
  END.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-3 C-Win
ON CHOOSE OF BUTTON-3 IN FRAME fDetail /* Button 3 */
DO:
    DEFINE VARIABLE cState AS CHARACTER   NO-UNDO.
    DEFINE VARIABLE cDummy AS CHARACTER   NO-UNDO.
  RUN sr/wSearch.w ("State","State", "state",?, OUTPUT cState, OUTPUT cDummy).
  
  IF cState <> "" AND cState <> ? THEN
  DO:
     DISPLAY 
         cState @ customer.State
         WITH FRAME fDetail.
         
         
  END.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME DEFAULT-FRAME
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  DISABLE ALL WITH FRAME fDetail.
  RUN ipSetButtonState (FALSE).
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE btnCancel RECT-1 btnSearch btnSave btnAdd btnEdit btnDelete btnOrders 
         BROWSE-2 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}

  {&OPEN-QUERY-fDetail}
  GET FIRST fDetail.
  IF AVAILABLE Customer THEN 
    DISPLAY Customer.Name Customer.CustNum Customer.Address Customer.Address2 
          Customer.City Customer.PostalCode Customer.State Customer.Country 
          Customer.Contact Customer.EmailAddress Customer.Phone Customer.Fax 
          Customer.SalesRep Customer.CreditLimit Customer.Discount 
          Customer.Balance Customer.Terms Customer.Comments 
      WITH FRAME fDetail IN WINDOW C-Win.
  IF AVAILABLE Salesrep THEN 
    DISPLAY Salesrep.RepName 
      WITH FRAME fDetail IN WINDOW C-Win.
  ENABLE BUTTON-1 RECT-2 RECT-3 RECT-4 RECT-5 RECT-6 Customer.Name 
         Customer.CustNum Customer.Address Customer.Address2 Customer.City 
         Customer.PostalCode Customer.State Customer.Country Customer.Contact 
         Customer.EmailAddress BUTTON-2 Customer.Phone Customer.Fax 
         Salesrep.RepName Customer.SalesRep Customer.CreditLimit 
         Customer.Discount BUTTON-3 Customer.Balance Customer.Terms 
         Customer.Comments 
      WITH FRAME fDetail IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fDetail}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ipAdd C-Win 
PROCEDURE ipAdd :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 CREATE customer.
 DISPLAY customer WITH FRAME fDetail.
 ENABLE ALL WITH FRAME fDetail.
 RUN ipSetButtonState(TRUE).
 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ipDelete C-Win 
PROCEDURE ipDelete :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  DEFINE VARIABLE lChoice AS LOGICAL     NO-UNDO.

  IF AVAILABLE Customer THEN
  DO:
    MESSAGE "Do you really want to delete customer " customer.NAME "?"  
        VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO UPDATE lChoice.
        
    IF lChoice THEN
      DO TRANSACTION:
          FIND CURRENT customer EXCLUSIVE-LOCK NO-ERROR NO-WAIT.
          IF AVAILABLE Customer THEN
          DO:
             DELETE customer.  
          END.
      END.    
  END.
  


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ipSetButtonState C-Win 
PROCEDURE ipSetButtonState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEFINE INPUT  PARAMETER plEditMode AS LOGICAL     NO-UNDO.

DO WITH FRAME DEFAULT-FRAME:
   btnCancel:HIDDEN = NOT plEditMode.
   btnSave:HIDDEN = NOT plEditMode.
   
   btnSearch:HIDDEN = plEditMode.
   btnAdd:HIDDEN = plEditMode.
   btnEdit:HIDDEN = plEditMode.
   btnDelete:HIDDEN = plEditMode.
   btnOrders:HIDDEN = plEditMode.
   browse-2:SENSITIVE = NOT plEditMode.
END.

IF plEditMode THEN
DO:
    ENABLE ALL WITH FRAME fDetail.    
END.
ELSE DO:
    DISABLE ALL WITH FRAME fDetail.
END.



END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

