&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          sports2000       PROGRESS
*/
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
DEFINE INPUT  PARAMETER iCustNum AS INTEGER     NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwOrder

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES Order OrderLine Salesrep

/* Definitions for BROWSE brwOrder                                      */
&Scoped-define FIELDS-IN-QUERY-brwOrder Order.Carrier Order.Creditcard ~
Order.Instructions Order.OrderDate Order.Ordernum Order.OrderStatus ~
Order.PO Order.PromiseDate Order.SalesRep Order.ShipDate Order.ShipToID ~
Order.Terms Order.WarehouseNum 
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwOrder 
&Scoped-define QUERY-STRING-brwOrder FOR EACH Order NO-LOCK ~
    BY Order.OrderDate INDEXED-REPOSITION
&Scoped-define OPEN-QUERY-brwOrder OPEN QUERY brwOrder FOR EACH Order NO-LOCK ~
    BY Order.OrderDate INDEXED-REPOSITION.
&Scoped-define TABLES-IN-QUERY-brwOrder Order
&Scoped-define FIRST-TABLE-IN-QUERY-brwOrder Order


/* Definitions for BROWSE brwOrderLine                                  */
&Scoped-define FIELDS-IN-QUERY-brwOrderLine OrderLine.Discount ~
OrderLine.ExtendedPrice OrderLine.Itemnum OrderLine.Linenum ~
OrderLine.OrderLineStatus OrderLine.Price OrderLine.Qty 
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwOrderLine 
&Scoped-define QUERY-STRING-brwOrderLine FOR EACH OrderLine NO-LOCK INDEXED-REPOSITION
&Scoped-define OPEN-QUERY-brwOrderLine OPEN QUERY brwOrderLine FOR EACH OrderLine NO-LOCK INDEXED-REPOSITION.
&Scoped-define TABLES-IN-QUERY-brwOrderLine OrderLine
&Scoped-define FIRST-TABLE-IN-QUERY-brwOrderLine OrderLine


/* Definitions for FRAME DEFAULT-FRAME                                  */

/* Definitions for FRAME fDetail                                        */
&Scoped-define FIELDS-IN-QUERY-fDetail Order.Ordernum Order.OrderStatus ~
Order.CustNum Order.OrderDate Order.Terms Order.SalesRep Salesrep.RepName ~
Order.ShipDate Order.Carrier Order.PO Order.Creditcard Order.PromiseDate ~
Order.BillToID Order.ShipToID Order.WarehouseNum Order.Instructions 
&Scoped-define ENABLED-FIELDS-IN-QUERY-fDetail Order.Ordernum ~
Order.OrderStatus Order.CustNum Order.OrderDate Order.Terms Order.SalesRep ~
Salesrep.RepName Order.ShipDate Order.Carrier Order.PO Order.Creditcard ~
Order.PromiseDate Order.BillToID Order.ShipToID Order.WarehouseNum ~
Order.Instructions 
&Scoped-define ENABLED-TABLES-IN-QUERY-fDetail Order Salesrep
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-fDetail Order
&Scoped-define SECOND-ENABLED-TABLE-IN-QUERY-fDetail Salesrep
&Scoped-define QUERY-STRING-fDetail FOR EACH Order SHARE-LOCK, ~
      EACH Salesrep OF Order SHARE-LOCK
&Scoped-define OPEN-QUERY-fDetail OPEN QUERY fDetail FOR EACH Order SHARE-LOCK, ~
      EACH Salesrep OF Order SHARE-LOCK.
&Scoped-define TABLES-IN-QUERY-fDetail Order Salesrep
&Scoped-define FIRST-TABLE-IN-QUERY-fDetail Order
&Scoped-define SECOND-TABLE-IN-QUERY-fDetail Salesrep


/* Definitions for FRAME fOrderLine                                     */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS btnCancel RECT-1 btnSearch btnSave btnAdd ~
btnEdit btnDelete tabDetails cCustomer iTotalOrders dAmount brwOrder ~
tabOrderLines 
&Scoped-Define DISPLAYED-OBJECTS cCustomer iTotalOrders dAmount 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON btnAdd 
     IMAGE-UP FILE "C:/PUG2019/src/crm/icons/add.ico":U
     LABEL "Button 3" 
     SIZE 8 BY 1.91 TOOLTIP "Add Customer".

DEFINE BUTTON btnCancel 
     IMAGE-UP FILE "C:/PUG2019/src/crm/icons/sign_stop.ico":U
     LABEL "Button 7" 
     SIZE 8 BY 1.91 TOOLTIP "Cancel".

DEFINE BUTTON btnDelete 
     IMAGE-UP FILE "C:/PUG2019/src/crm/icons/delete.ico":U
     LABEL "Button 5" 
     SIZE 8 BY 1.91 TOOLTIP "Delete Customer".

DEFINE BUTTON btnEdit 
     IMAGE-UP FILE "C:/PUG2019/src/crm/icons/edit.ico":U
     LABEL "" 
     SIZE 8 BY 1.91 TOOLTIP "Edit Customer".

DEFINE BUTTON btnSave 
     IMAGE-UP FILE "C:/PUG2019/src/crm/icons/data_floppy_disk.ico":U
     LABEL "" 
     SIZE 8 BY 1.91 TOOLTIP "Save".

DEFINE BUTTON btnSearch 
     IMAGE-UP FILE "C:/PUG2019/src/crm/icons/magnifying_glass.ico":U
     LABEL "Button 1" 
     SIZE 8 BY 1.91 TOOLTIP "Find Customer".

DEFINE BUTTON tabDetails  NO-FOCUS NO-CONVERT-3D-COLORS
     LABEL "Details" 
     SIZE 10.4 BY .86.

DEFINE BUTTON tabOrderLines 
     LABEL "OrderLines" 
     SIZE 14 BY .86.

DEFINE VARIABLE cCustomer AS CHARACTER FORMAT "X(256)":U 
     LABEL "Customer" 
     VIEW-AS FILL-IN 
     SIZE 101 BY 1 NO-UNDO.

DEFINE VARIABLE dAmount AS DECIMAL FORMAT "->>,>>9.99":U INITIAL 0 
     LABEL "Amount" 
     VIEW-AS FILL-IN 
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE iTotalOrders AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "Number of orders" 
     VIEW-AS FILL-IN 
     SIZE 21 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 250 BY .05.

DEFINE BUTTON BUTTON-1 
     IMAGE-UP FILE "sr/magnifying_glass.jpg":U NO-FOCUS FLAT-BUTTON NO-CONVERT-3D-COLORS
     LABEL "Button 1" 
     SIZE 4.6 BY 1.1.

DEFINE BUTTON BUTTON-10 
     IMAGE-UP FILE "sr/magnifying_glass.jpg":U NO-FOCUS FLAT-BUTTON NO-CONVERT-3D-COLORS
     LABEL "Button 10" 
     SIZE 4.6 BY 1.1.

DEFINE BUTTON BUTTON-11 
     IMAGE-UP FILE "sr/magnifying_glass.jpg":U NO-FOCUS FLAT-BUTTON NO-CONVERT-3D-COLORS
     LABEL "Button 11" 
     SIZE 4.6 BY 1.1.

DEFINE BUTTON BUTTON-7 
     IMAGE-UP FILE "sr/magnifying_glass.jpg":U NO-FOCUS FLAT-BUTTON NO-CONVERT-3D-COLORS
     LABEL "Button 7" 
     SIZE 4.6 BY 1.1.

DEFINE BUTTON BUTTON-8 
     IMAGE-UP FILE "sr/magnifying_glass.jpg":U NO-FOCUS FLAT-BUTTON NO-CONVERT-3D-COLORS
     LABEL "Button 8" 
     SIZE 4.6 BY 1.1.

DEFINE BUTTON BUTTON-9 
     IMAGE-UP FILE "sr/magnifying_glass.jpg":U NO-FOCUS FLAT-BUTTON NO-CONVERT-3D-COLORS
     LABEL "Button 9" 
     SIZE 4.6 BY 1.1.

DEFINE BUTTON BUTTON-2 
     IMAGE-UP FILE "sr/magnifying_glass.jpg":U NO-FOCUS FLAT-BUTTON NO-CONVERT-3D-COLORS
     LABEL "Button 2" 
     SIZE 4.6 BY 1.1.

DEFINE VARIABLE cItemName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 84 BY 1 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwOrder FOR 
      Order SCROLLING.

DEFINE QUERY brwOrderLine FOR 
      OrderLine SCROLLING.

DEFINE QUERY fDetail FOR 
      Order, 
      Salesrep SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwOrder
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwOrder C-Win _STRUCTURED
  QUERY brwOrder NO-LOCK DISPLAY
      Order.Carrier FORMAT "x(25)":U
      Order.Creditcard FORMAT "x(20)":U
      Order.Instructions FORMAT "x(50)":U
      Order.OrderDate FORMAT "99/99/99":U
      Order.Ordernum FORMAT "zzzzzzzzz9":U
      Order.OrderStatus FORMAT "x(20)":U
      Order.PO FORMAT "x(20)":U
      Order.PromiseDate FORMAT "99/99/99":U
      Order.SalesRep FORMAT "x(4)":U
      Order.ShipDate FORMAT "99/99/9999":U
      Order.ShipToID FORMAT "zzzzzzzzz9":U
      Order.Terms FORMAT "x(20)":U
      Order.WarehouseNum FORMAT "zzzzzzzzz9":U
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 120 BY 28.76 FIT-LAST-COLUMN.

DEFINE BROWSE brwOrderLine
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwOrderLine C-Win _STRUCTURED
  QUERY brwOrderLine NO-LOCK DISPLAY
      OrderLine.Discount FORMAT ">>9%":U
      OrderLine.ExtendedPrice FORMAT "->>>,>>9.99":U
      OrderLine.Itemnum FORMAT "zzzzzzzzz9":U
      OrderLine.Linenum FORMAT ">>9":U
      OrderLine.OrderLineStatus FORMAT "x(20)":U
      OrderLine.Price FORMAT "->,>>>,>>9.99":U
      OrderLine.Qty FORMAT "->>>>9":U WIDTH 42.4
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 127 BY 13.57 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     btnCancel AT ROW 1 COL 1 WIDGET-ID 16
     btnSearch AT ROW 1 COL 2 WIDGET-ID 26
     btnSave AT ROW 1 COL 9 WIDGET-ID 18
     btnAdd AT ROW 1 COL 11 WIDGET-ID 6
     btnEdit AT ROW 1 COL 20 WIDGET-ID 8
     btnDelete AT ROW 1 COL 29.2 WIDGET-ID 10
     tabDetails AT ROW 3.24 COL 121.6 WIDGET-ID 20
     cCustomer AT ROW 1.48 COL 48 COLON-ALIGNED WIDGET-ID 24
     iTotalOrders AT ROW 1.48 COL 169 COLON-ALIGNED WIDGET-ID 28
     dAmount AT ROW 1.52 COL 200.6 COLON-ALIGNED WIDGET-ID 30
     brwOrder AT ROW 3.19 COL 1 WIDGET-ID 200
     tabOrderLines AT ROW 3.24 COL 131.8 WIDGET-ID 22
     RECT-1 AT ROW 2.95 COL 1 WIDGET-ID 14
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 250.6 BY 31.05 WIDGET-ID 100.

DEFINE FRAME fOrderLine
     brwOrderLine AT ROW 1.24 COL 2 WIDGET-ID 500
     BUTTON-2 AT ROW 16.43 COL 35.8 WIDGET-ID 58
     OrderLine.Linenum AT ROW 15.29 COL 18 COLON-ALIGNED WIDGET-ID 8
          VIEW-AS FILL-IN 
          SIZE 6.2 BY 1
     OrderLine.Ordernum AT ROW 15.29 COL 42.8 COLON-ALIGNED WIDGET-ID 12
          VIEW-AS FILL-IN 
          SIZE 16 BY 1
     OrderLine.OrderLineStatus AT ROW 15.29 COL 101 COLON-ALIGNED WIDGET-ID 10
          VIEW-AS COMBO-BOX 
          LIST-ITEMS "Ordered","Back Ordered","Shipped","Approval Pending" 
          DROP-DOWN-LIST
          SIZE 25.8 BY 1
     OrderLine.Itemnum AT ROW 16.48 COL 18 COLON-ALIGNED WIDGET-ID 6
          VIEW-AS FILL-IN 
          SIZE 16 BY 1
     cItemName AT ROW 16.48 COL 42.8 COLON-ALIGNED NO-LABEL WIDGET-ID 62
     OrderLine.Price AT ROW 17.67 COL 18 COLON-ALIGNED WIDGET-ID 14
          VIEW-AS FILL-IN 
          SIZE 20.2 BY 1
     OrderLine.Qty AT ROW 18.86 COL 18 COLON-ALIGNED WIDGET-ID 16
          VIEW-AS FILL-IN 
          SIZE 20.2 BY 1
     OrderLine.Discount AT ROW 20.05 COL 18 COLON-ALIGNED WIDGET-ID 2
          VIEW-AS FILL-IN 
          SIZE 20.2 BY 1
     OrderLine.ExtendedPrice AT ROW 21.29 COL 18 COLON-ALIGNED WIDGET-ID 4
          VIEW-AS FILL-IN 
          SIZE 20.2 BY 1
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 121.8 ROW 4.05
         SIZE 129.2 BY 27.91 WIDGET-ID 400.

DEFINE FRAME fDetail
     BUTTON-1 AT ROW 4.91 COL 30.8 WIDGET-ID 56
     Order.Ordernum AT ROW 1.38 COL 19 COLON-ALIGNED WIDGET-ID 14
          VIEW-AS FILL-IN 
          SIZE 30 BY 1
     Order.OrderStatus AT ROW 1.43 COL 99.2 COLON-ALIGNED WIDGET-ID 16
          VIEW-AS COMBO-BOX 
          LIST-ITEMS "Ordered","Back Ordered","Partially Shipped","Shipped","Approval Pending" 
          DROP-DOWN-LIST
          SIZE 26.8 BY 1
     Order.CustNum AT ROW 2.57 COL 19 COLON-ALIGNED WIDGET-ID 8
          VIEW-AS FILL-IN 
          SIZE 30 BY 1
     Order.OrderDate AT ROW 2.67 COL 99.2 COLON-ALIGNED WIDGET-ID 12 FORMAT "99/99/9999"
          VIEW-AS FILL-IN 
          SIZE 26.8 BY 1
     Order.Terms AT ROW 3.76 COL 99.2 COLON-ALIGNED WIDGET-ID 28
          VIEW-AS FILL-IN 
          SIZE 26.8 BY 1
     Order.SalesRep AT ROW 4.95 COL 19 COLON-ALIGNED WIDGET-ID 22
          VIEW-AS FILL-IN 
          SIZE 9.6 BY 1
     Salesrep.RepName AT ROW 4.95 COL 39.4 COLON-ALIGNED NO-LABEL WIDGET-ID 34
          VIEW-AS FILL-IN 
          SIZE 86.4 BY 1
     Order.ShipDate AT ROW 6.95 COL 99 COLON-ALIGNED WIDGET-ID 24
          VIEW-AS FILL-IN 
          SIZE 26.8 BY 1
     Order.Carrier AT ROW 7 COL 19 COLON-ALIGNED WIDGET-ID 4
          VIEW-AS FILL-IN 
          SIZE 25.8 BY 1
     Order.PO AT ROW 8.19 COL 99 COLON-ALIGNED WIDGET-ID 18
          VIEW-AS FILL-IN 
          SIZE 22.2 BY 1
     Order.Creditcard AT ROW 8.33 COL 19 COLON-ALIGNED WIDGET-ID 6
          VIEW-AS COMBO-BOX 
          LIST-ITEMS "Visa","American Express","Master Card" 
          DROP-DOWN-LIST
          SIZE 30 BY 1
     Order.PromiseDate AT ROW 9.33 COL 99 COLON-ALIGNED WIDGET-ID 20
          VIEW-AS FILL-IN 
          SIZE 26.8 BY 1
     Order.BillToID AT ROW 10.05 COL 19 COLON-ALIGNED WIDGET-ID 2
          VIEW-AS FILL-IN 
          SIZE 25.8 BY 1
     Order.ShipToID AT ROW 11.48 COL 19 COLON-ALIGNED WIDGET-ID 26
          VIEW-AS FILL-IN 
          SIZE 25.8 BY 1
     Order.WarehouseNum AT ROW 12.91 COL 19 COLON-ALIGNED WIDGET-ID 30
          VIEW-AS FILL-IN 
          SIZE 25.8 BY 1
     Order.Instructions AT ROW 14.57 COL 21 NO-LABEL WIDGET-ID 32
          VIEW-AS EDITOR SCROLLBAR-VERTICAL
          SIZE 106.4 BY 13.57
     BUTTON-11 AT ROW 7 COL 46.8 WIDGET-ID 80
     BUTTON-10 AT ROW 11.48 COL 46.8 WIDGET-ID 76
     BUTTON-9 AT ROW 12.86 COL 46.8 WIDGET-ID 74
     BUTTON-8 AT ROW 10 COL 46.8 WIDGET-ID 72
     BUTTON-7 AT ROW 8.14 COL 123.2 WIDGET-ID 70
     "Comments:" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 14.48 COL 9.2 WIDGET-ID 78
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 122 ROW 4.05
         SIZE 129.2 BY 27.91 WIDGET-ID 300.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Order Maintenance"
         HEIGHT             = 31.05
         WIDTH              = 252
         MAX-HEIGHT         = 35.62
         MAX-WIDTH          = 252.8
         VIRTUAL-HEIGHT     = 35.62
         VIRTUAL-WIDTH      = 252.8
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

&IF '{&WINDOW-SYSTEM}' NE 'TTY' &THEN
IF NOT C-Win:LOAD-ICON("C:/PUG2019/src/crm/icons/dude.ico":U) THEN
    MESSAGE "Unable to load icon: C:/PUG2019/src/crm/icons/dude.ico"
            VIEW-AS ALERT-BOX WARNING BUTTONS OK.
&ENDIF
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME fDetail:FRAME = FRAME DEFAULT-FRAME:HANDLE
       FRAME fOrderLine:FRAME = FRAME DEFAULT-FRAME:HANDLE.

/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* BROWSE-TAB brwOrder dAmount DEFAULT-FRAME */
ASSIGN 
       cCustomer:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       dAmount:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       iTotalOrders:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR FRAME fDetail
                                                                        */
ASSIGN 
       Order.CustNum:READ-ONLY IN FRAME fDetail        = TRUE.

/* SETTINGS FOR FILL-IN Order.OrderDate IN FRAME fDetail
   EXP-FORMAT                                                           */
ASSIGN 
       Order.Ordernum:READ-ONLY IN FRAME fDetail        = TRUE.

/* SETTINGS FOR FILL-IN Salesrep.RepName IN FRAME fDetail
   EXP-LABEL                                                            */
ASSIGN 
       Salesrep.RepName:READ-ONLY IN FRAME fDetail        = TRUE.

/* SETTINGS FOR FRAME fOrderLine
                                                                        */
/* BROWSE-TAB brwOrderLine 1 fOrderLine */
ASSIGN 
       cItemName:READ-ONLY IN FRAME fOrderLine        = TRUE.

ASSIGN 
       OrderLine.Linenum:READ-ONLY IN FRAME fOrderLine        = TRUE.

ASSIGN 
       OrderLine.Ordernum:READ-ONLY IN FRAME fOrderLine        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwOrder
/* Query rebuild information for BROWSE brwOrder
     _TblList          = "sports2000.Order"
     _Options          = "NO-LOCK INDEXED-REPOSITION"
     _OrdList          = "sports2000.Order.OrderDate|yes"
     _FldNameList[1]   = sports2000.Order.Carrier
     _FldNameList[2]   = sports2000.Order.Creditcard
     _FldNameList[3]   = sports2000.Order.Instructions
     _FldNameList[4]   = sports2000.Order.OrderDate
     _FldNameList[5]   = sports2000.Order.Ordernum
     _FldNameList[6]   = sports2000.Order.OrderStatus
     _FldNameList[7]   = sports2000.Order.PO
     _FldNameList[8]   = sports2000.Order.PromiseDate
     _FldNameList[9]   = sports2000.Order.SalesRep
     _FldNameList[10]   = sports2000.Order.ShipDate
     _FldNameList[11]   = sports2000.Order.ShipToID
     _FldNameList[12]   = sports2000.Order.Terms
     _FldNameList[13]   = sports2000.Order.WarehouseNum
     _Query            is NOT OPENED
*/  /* BROWSE brwOrder */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwOrderLine
/* Query rebuild information for BROWSE brwOrderLine
     _TblList          = "sports2000.OrderLine"
     _Options          = "NO-LOCK INDEXED-REPOSITION"
     _FldNameList[1]   = sports2000.OrderLine.Discount
     _FldNameList[2]   = sports2000.OrderLine.ExtendedPrice
     _FldNameList[3]   = sports2000.OrderLine.Itemnum
     _FldNameList[4]   = sports2000.OrderLine.Linenum
     _FldNameList[5]   = sports2000.OrderLine.OrderLineStatus
     _FldNameList[6]   = sports2000.OrderLine.Price
     _FldNameList[7]   > sports2000.OrderLine.Qty
"OrderLine.Qty" ? ? "integer" ? ? ? ? ? ? no ? no no "42.4" yes no no "U" "" "" "" "" "" "" 0 no 0 no no
     _Query            is NOT OPENED
*/  /* BROWSE brwOrderLine */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fDetail
/* Query rebuild information for FRAME fDetail
     _TblList          = "sports2000.Order,sports2000.Salesrep OF sports2000.Order"
     _Query            is OPENED
*/  /* FRAME fDetail */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Order Maintenance */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Order Maintenance */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwOrder
&Scoped-define SELF-NAME brwOrder
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwOrder C-Win
ON VALUE-CHANGED OF brwOrder IN FRAME DEFAULT-FRAME
DO:
  IF FRAME fOrderLine:HIDDEN = FALSE THEN DO:
    IF AVAILABLE order THEN
    DO:
       CLOSE QUERY brwOrderLine.
       OPEN QUERY brwOrderLine FOR EACH OrderLine WHERE OrderLine.OrderNum = Order.OrderNum.
       APPLY "value-changed" TO brwOrderLine.
    END.
  END.
  ELSE DO:  
    IF AVAILABLE order THEN
    DO:
       DISPLAY order WITH FRAME fDetail.  
       FIND salesrep OF order NO-ERROR.
       IF AVAILABLE salesrep THEN
       DO:
           DISPLAY salesrep.repname WITH FRAME fDetail.           
       END.
    END.
  END.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwOrderLine
&Scoped-define FRAME-NAME fOrderLine
&Scoped-define SELF-NAME brwOrderLine
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwOrderLine C-Win
ON VALUE-CHANGED OF brwOrderLine IN FRAME fOrderLine
DO:
  
  IF AVAILABLE orderline THEN
  DO:
     DISPLAY orderline WITH FRAME fOrderLine. 
     FIND ITEM OF orderline NO-LOCK.
     DISPLAY ITEM.itemNAME @ cItemName WITH FRAME fOrderLine.
  END.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define SELF-NAME btnAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btnAdd C-Win
ON CHOOSE OF btnAdd IN FRAME DEFAULT-FRAME /* Button 3 */
DO:
  RUN ipAdd.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btnCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btnCancel C-Win
ON CHOOSE OF btnCancel IN FRAME DEFAULT-FRAME /* Button 7 */
DO:
  RUN ipSetButtonState(FALSE).
  DISABLE ALL WITH FRAME fDetail.
  DISABLE ALL WITH FRAME fOrderDetail. 
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btnDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btnDelete C-Win
ON CHOOSE OF btnDelete IN FRAME DEFAULT-FRAME /* Button 5 */
DO:
  RUN ipDelete.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btnEdit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btnEdit C-Win
ON CHOOSE OF btnEdit IN FRAME DEFAULT-FRAME
DO:

RUN ipEdit.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btnSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btnSave C-Win
ON CHOOSE OF btnSave IN FRAME DEFAULT-FRAME
DO:
  RUN ipSave.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btnSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btnSearch C-Win
ON CHOOSE OF btnSearch IN FRAME DEFAULT-FRAME /* Button 1 */
DO:
  RUN ipSearch.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fDetail
&Scoped-define SELF-NAME BUTTON-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-1 C-Win
ON CHOOSE OF BUTTON-1 IN FRAME fDetail /* Button 1 */
DO:
  DEFINE VARIABLE cSalesRep AS CHARACTER   NO-UNDO.
  DEFINE VARIABLE cRepName AS CHARACTER   NO-UNDO.
  RUN sr/wSearch.w ("SalesRep","Sales Representatives", "salesrep","repname", OUTPUT cSalesrep, OUTPUT cRepName).
  
  IF cSalesRep <> "" AND cSalesRep <> ? THEN
  DO:
     DISPLAY 
         cSalesRep @ order.Salesrep
         cRepname  @ salesrep.Repname
         WITH FRAME fDetail.
  
     APPLY "value-changed" TO order.Salesrep IN FRAME fDetail.
  END.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-10
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-10 C-Win
ON CHOOSE OF BUTTON-10 IN FRAME fDetail /* Button 10 */
DO:
  DEFINE VARIABLE cKeyField    AS CHARACTER   NO-UNDO.
  DEFINE VARIABLE cDescription AS CHARACTER   NO-UNDO.
  
  RUN sr/wSearch.w ("ShipTo","Ship To", "ShipToID",?, OUTPUT cKeyField, OUTPUT cDescription).
  
  IF cKeyField <> "" AND cKeyField <> ? THEN
  DO:
     DISPLAY 
         cKeyField @ order.ShipToId
         WITH FRAME fDetail.
  
     APPLY "value-changed" TO order.ShipToId IN FRAME fDetail.
  END.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-11
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-11 C-Win
ON CHOOSE OF BUTTON-11 IN FRAME fDetail /* Button 11 */
DO:
  DEFINE VARIABLE cKeyField    AS CHARACTER   NO-UNDO.
  DEFINE VARIABLE cDescription AS CHARACTER   NO-UNDO.
  
  
  
  RUN sr/wSearch.w ("Supplier ","Carrier", "Name",?, OUTPUT cKeyField, OUTPUT cDescription).
  
  IF cKeyField <> "" AND cKeyField <> ? THEN
  DO:
     DISPLAY 
         cKeyField @ order.Carrier
         WITH FRAME fDetail.
  
     APPLY "value-changed" TO order.Carrier IN FRAME fDetail.
  END.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fOrderLine
&Scoped-define SELF-NAME BUTTON-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-2 C-Win
ON CHOOSE OF BUTTON-2 IN FRAME fOrderLine /* Button 2 */
DO:
    DEFINE VARIABLE cItemNum AS CHARACTER   NO-UNDO.
    DEFINE VARIABLE cDummy AS CHARACTER   NO-UNDO.
  RUN sr/wSearch.w ("item","item", "itemnum","itemname", OUTPUT cItemNum, OUTPUT cDummy).
  
  IF cItemNum <> "" AND cItemNum <> ? THEN
  DO:
     DISPLAY 
         cItemNum @ orderline.itemnum
         cDummy @ cItemName
         WITH FRAME fOrderLine.
         
         
  END.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fDetail
&Scoped-define SELF-NAME BUTTON-7
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-7 C-Win
ON CHOOSE OF BUTTON-7 IN FRAME fDetail /* Button 7 */
DO:
  DEFINE VARIABLE cPO AS CHARACTER   NO-UNDO.
  DEFINE VARIABLE cDummy AS CHARACTER   NO-UNDO.
  RUN sr/wSearch.w ("PurchaseOrder","Purchase Order", "ponum",?, OUTPUT cPO, OUTPUT cDummy).
  
  IF cPO <> "" AND cPO <> ? THEN
  DO:
     DISPLAY 
         cPO @ order.po
         WITH FRAME fDetail.
  
     APPLY "value-changed" TO order.po IN FRAME fDetail.
  END.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-8
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-8 C-Win
ON CHOOSE OF BUTTON-8 IN FRAME fDetail /* Button 8 */
DO:
  DEFINE VARIABLE cKeyField    AS CHARACTER   NO-UNDO.
  DEFINE VARIABLE cDescription AS CHARACTER   NO-UNDO.
  
  RUN sr/wSearch.w ("BillTo","Bill To", "BillToID","name", OUTPUT cKeyField, OUTPUT cDescription).
  
  IF cKeyField <> "" AND cKeyField <> ? THEN
  DO:
     DISPLAY 
         cKeyField @ order.BillToId
         WITH FRAME fDetail.
  
     APPLY "value-changed" TO order.BillToId IN FRAME fDetail.
  END.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-9
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-9 C-Win
ON CHOOSE OF BUTTON-9 IN FRAME fDetail /* Button 9 */
DO:
  DEFINE VARIABLE cKeyField    AS CHARACTER   NO-UNDO.
  DEFINE VARIABLE cDescription AS CHARACTER   NO-UNDO.
  
  RUN sr/wSearch.w ("Warehouse","Warehouse", "WarehouseNum",?, OUTPUT cKeyField, OUTPUT cDescription).
  
  IF cKeyField <> "" AND cKeyField <> ? THEN
  DO:
     DISPLAY 
         cKeyField @ order.WarehouseNum
         WITH FRAME fDetail.
  
     APPLY "value-changed" TO order.WarehouseNum IN FRAME fDetail.
  END.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define SELF-NAME tabDetails
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tabDetails C-Win
ON CHOOSE OF tabDetails IN FRAME DEFAULT-FRAME /* Details */
DO:
  FRAME fOrderLine:HIDDEN = TRUE.
  FRAME fDetail:HIDDEN = FALSE.
  APPLY "value-changed" TO brwOrder.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tabOrderLines
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tabOrderLines C-Win
ON CHOOSE OF tabOrderLines IN FRAME DEFAULT-FRAME /* OrderLines */
DO:
  FRAME fOrderLine:HIDDEN = FALSE.
  FRAME fDetail:HIDDEN   = TRUE.
  APPLY "value-changed" TO brwOrder.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwOrder
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.


       
/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
    RUN enable_UI.

    APPLY "choose" TO tabDetails.
    RUN ipSetButtonState (FALSE).  
  
    IF iCustNum <> 0 AND iCustnum <> ? THEN DO:
     FIND Customer WHERE Customer.custnum = iCustNum NO-LOCK NO-ERROR.
     IF AVAILABLE Customer THEN DO:
        RUN ipQueryOrders(ROWID(Customer)).
     END.
        
    END.
    ELSE RUN ipSearch.  
         
  
    IF NOT THIS-PROCEDURE:PERSISTENT THEN
      WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cCustomer iTotalOrders dAmount 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE btnCancel RECT-1 btnSearch btnSave btnAdd btnEdit btnDelete tabDetails 
         cCustomer iTotalOrders dAmount brwOrder tabOrderLines 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  DISPLAY cItemName 
      WITH FRAME fOrderLine IN WINDOW C-Win.
  IF AVAILABLE OrderLine THEN 
    DISPLAY OrderLine.Linenum OrderLine.Ordernum OrderLine.OrderLineStatus 
          OrderLine.Itemnum OrderLine.Price OrderLine.Qty OrderLine.Discount 
          OrderLine.ExtendedPrice 
      WITH FRAME fOrderLine IN WINDOW C-Win.
  ENABLE brwOrderLine BUTTON-2 OrderLine.Linenum OrderLine.Ordernum 
         OrderLine.OrderLineStatus OrderLine.Itemnum cItemName OrderLine.Price 
         OrderLine.Qty OrderLine.Discount OrderLine.ExtendedPrice 
      WITH FRAME fOrderLine IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fOrderLine}

  {&OPEN-QUERY-fDetail}
  GET FIRST fDetail.
  IF AVAILABLE Order THEN 
    DISPLAY Order.Ordernum Order.OrderStatus Order.CustNum Order.OrderDate 
          Order.Terms Order.SalesRep Order.ShipDate Order.Carrier Order.PO 
          Order.Creditcard Order.PromiseDate Order.BillToID Order.ShipToID 
          Order.WarehouseNum Order.Instructions 
      WITH FRAME fDetail IN WINDOW C-Win.
  IF AVAILABLE Salesrep THEN 
    DISPLAY Salesrep.RepName 
      WITH FRAME fDetail IN WINDOW C-Win.
  ENABLE BUTTON-1 Order.Ordernum Order.OrderStatus Order.CustNum 
         Order.OrderDate Order.Terms Order.SalesRep Salesrep.RepName 
         Order.ShipDate Order.Carrier Order.PO Order.Creditcard 
         Order.PromiseDate Order.BillToID Order.ShipToID Order.WarehouseNum 
         Order.Instructions BUTTON-11 BUTTON-10 BUTTON-9 BUTTON-8 BUTTON-7 
      WITH FRAME fDetail IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fDetail}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ipAdd C-Win 
PROCEDURE ipAdd :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEFINE VARIABLE iOrderNummer AS INTEGER     NO-UNDO.
DEFINE VARIABLE iLineNumber  AS INTEGER     NO-UNDO.

DEFINE BUFFER bOrderLine FOR OrderLine.


IF FRAME fOrderLine:HIDDEN = TRUE THEN
DO:
    CREATE order.                    
    
    order.custnum = iCustnum. 
    
    DISPLAY order WITH FRAME fDetail.
    ENABLE ALL WITH FRAME fDetail.      
    RUN ipSetButtonState(TRUE).             
END.
ELSE DO:
    FIND LAST bOrderLine NO-LOCK WHERE Order.OrderNum = bOrderLine.OrderNum USE-INDEX orderline NO-ERROR.
    IF AVAILABLE bOrderLine THEN iLineNumber = bOrderline.Linenum + 1.
    ELSE iLineNumber = 1.

    CREATE orderline.                    
    ASSIGN
         OrderLine.ordernum = order.ordernum
         OrderLine.Linenum  = iLineNumber.
         
    DISPLAY orderline WITH FRAME fOrderLine.
    ENABLE ALL WITH FRAME fOrderline.      
    RUN ipSetButtonState(TRUE).    
END.

 



END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ipCalculateTotals C-Win 
PROCEDURE ipCalculateTotals :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEFINE BUFFER bOrder FOR Order.
DEFINE BUFFER bOrderLine FOR OrderLine.

DEFINE INPUT  PARAMETER piCustNum AS INTEGER     NO-UNDO.

DEFINE VARIABLE iNumOrders      AS INTEGER     NO-UNDO.
DEFINE VARIABLE dAmountOrdered  AS DECIMAL     NO-UNDO.

FOR EACH bOrder WHERE bOrder.custnum = piCustnum NO-LOCK:
    iNumOrders = iNumOrders + 1.
    
    FOR EACH bOrderLine OF bOrder NO-LOCK:
        dAmountOrdered = dAmountOrdered + ((bOrderLine.Price * bOrderLine.Qty) - ( ((bOrderLine.Discount / 100) * (bOrderLine.Price * bOrderLine.Qty)))). 
    END.
END.

DISPLAY iNumOrders        @ iTotalOrders WITH FRAME {&FRAME-NAME}.
DISPLAY dAmountOrdered    @ dAmount      WITH FRAME {&FRAME-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ipDelete C-Win 
PROCEDURE ipDelete :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  DEFINE VARIABLE lChoice AS LOGICAL     NO-UNDO.

    IF FRAME fOrderLine:HIDDEN = TRUE THEN DO:
      IF AVAILABLE order THEN
      DO:
        MESSAGE "Delete order?" 
            VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO UPDATE lChoice.
            
        IF lChoice THEN
        DO TRANSACTION:
           FIND CURRENT order EXCLUSIVE-LOCK NO-ERROR NO-WAIT.
           DELETE order.
        END.
        CLOSE QUERY brwOrder.
        OPEN QUERY brwOrder FOR EACH order WHERE order.custnum = iCustnum.       
      END.
  END.
  ELSE DO:
      IF AVAILABLE orderline THEN
      DO:
        MESSAGE "Delete order line?" 
            VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO UPDATE lChoice.
            
        IF lChoice THEN
        DO TRANSACTION:
           FIND CURRENT orderline EXCLUSIVE-LOCK NO-ERROR NO-WAIT.
           DELETE orderline.
        END.
      END.
      
      APPLY "value-changed" TO brwOrder IN FRAME {&FRAME-NAME}.
  END.
  
  

  


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ipEdit C-Win 
PROCEDURE ipEdit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  IF FRAME fOrderLine:HIDDEN = TRUE THEN DO:
      IF AVAILABLE order THEN
      DO:
        RUN ipSetButtonState(TRUE).
      END.
  END.
  ELSE DO:
      IF AVAILABLE orderline THEN
      DO:
        RUN ipSetButtonState(TRUE).
      END.
  END.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ipQueryOrders C-Win 
PROCEDURE ipQueryOrders :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEFINE INPUT  PARAMETER prCustomer AS ROWID       NO-UNDO.

  
  DEFINE BUFFER bCustomer FOR Customer.
  
  DO:
     FIND bCustomer WHERE ROWID(bCustomer)= prCustomer NO-LOCK NO-ERROR.
     IF AVAILABLE bCustomer THEN
     DO:
        cCustomer:SCREEN-VALUE IN FRAME {&frame-name} = bCustomer.NAME.
        
        RUN ipCalculateTotals (bCustomer.custnum).

        CLOSE QUERY brwOrder.
        OPEN QUERY brwOrder FOR EACH order WHERE order.custnum = bCustomer.custnum. 
        APPLY "value-changed" TO brwOrder.
     END.
  END.

  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ipSave C-Win 
PROCEDURE ipSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  IF FRAME fOrderLine:HIDDEN = TRUE THEN
  DO:
      DEFINE VARIABLE rOrder AS ROWID NO-UNDO.
      
      RUN ipSetButtonState(FALSE).
      
      DO TRANSACTION:
          FIND CURRENT Order EXCLUSIVE-LOCK NO-ERROR NO-WAIT.
          ASSIGN INPUT FRAME fDetail
            Order.Creditcard 
            Order.BillToID 
            Order.Carrier 
            Order.CustNum 
            Order.Instructions 
            Order.OrderDate 
            Order.Ordernum 
            Order.OrderStatus 
            Order.PO 
            Order.PromiseDate 
            Order.SalesRep 
            Order.ShipDate 
            Order.ShipToID 
            Order.Terms 
            Order.WarehouseNum.
      END.
     
      FIND CURRENT Order NO-LOCK NO-ERROR.
      rOrder = ROWID(Order) NO-ERROR.
      CLOSE QUERY brwOrder.
      OPEN QUERY brwOrder FOR EACH Order WHERE order.custnum = iCustnum BY OrderDate INDEXED-REPOSITION.
      IF rOrder <> ? THEN QUERY brwOrder:REPOSITION-TO-ROWID(rOrder).
  END.
  ELSE DO:
        DEFINE VARIABLE rOrderLine AS ROWID NO-UNDO.
        DEFINE VARIABLE iOrder AS INTEGER     NO-UNDO.
      
      RUN ipSetButtonState(FALSE).
      iOrder = IF AVAILABLE orderline THEN orderline.ordernum ELSE ?.
      
      DO TRANSACTION:
          FIND CURRENT OrderLine EXCLUSIVE-LOCK NO-ERROR NO-WAIT.
          ASSIGN INPUT FRAME fOrderLine
                OrderLine.Discount
                OrderLine.ExtendedPrice 
                OrderLine.Itemnum 
                OrderLine.Linenum 
                OrderLine.OrderLineStatus 
                OrderLine.Ordernum 
                OrderLine.Price 
                OrderLine.Qty 
          .
      END.
     
      FIND CURRENT OrderLine NO-LOCK NO-ERROR.
      rOrderLine = ROWID(OrderLine) NO-ERROR.
      
      CLOSE QUERY brwOrderLine.
      OPEN QUERY brwOrderLine FOR EACH OrderLine WHERE OrderLine.ordernum = iOrder INDEXED-REPOSITION.
      IF rOrderLine <> ? THEN QUERY brwOrderLine:REPOSITION-TO-ROWID(rOrderLine).
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ipSearch C-Win 
PROCEDURE ipSearch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  DEFINE VARIABLE cCustNum AS CHARACTER   NO-UNDO.
  DEFINE VARIABLE cDummy AS CHARACTER   NO-UNDO.
  
  DEFINE BUFFER bCustomer FOR Customer.
  
  RUN sr/wSearch.w ("Customer","Customers", "CustNum",?, OUTPUT cCustNum, OUTPUT cDummy).
  
  IF cCustNum <> ? AND cCustnum <> "" THEN
  DO:
     FIND bCustomer WHERE bCustomer.custnum = INTEGER(cCustNum) NO-LOCK NO-ERROR.
     IF AVAILABLE bCustomer THEN
     DO:
        icustnum = bCustomer.custnum.
        RUN ipQueryOrders(ROWID(bCustomer)).
     END.
  END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ipSetButtonState C-Win 
PROCEDURE ipSetButtonState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEFINE INPUT  PARAMETER plEditMode AS LOGICAL     NO-UNDO.

IF plEditMode THEN
DO:
    IF FRAME fDetail:VISIBLE   THEN ENABLE ALL WITH FRAME fDetail.    
    IF FRAME fOrderLine:VISIBLE THEN ENABLE ALL WITH FRAME fOrderLine.    
END.
ELSE DO:
    DISABLE ALL WITH FRAME fDetail.
    DISABLE ALL WITH FRAME fOrderLine.
    
END.
PROCESS EVENTS.

DO WITH FRAME DEFAULT-FRAME:

   ASSIGN 
       tabOrderLines:SENSITIVE          = NOT plEditMode   
       tabDetails:SENSITIVE             = NOT plEditMode
       BROWSE brwOrder:SENSITIVE        = NOT plEditMode
       BROWSE brwOrderLine:SENSITIVE    = NOT plEditMode
     
       btnCancel:HIDDEN                 = NOT plEditMode
       btnSave:HIDDEN                   = NOT plEditMode
       
       btnAdd:HIDDEN                    = plEditMode
       btnEdit:HIDDEN                   = plEditMode
       btnDelete:HIDDEN                 = plEditMode
   .
   
END.

    


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

