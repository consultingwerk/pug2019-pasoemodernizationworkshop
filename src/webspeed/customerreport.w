{src/web/method/e4gl.i} 
DEFINE VARIABLE iCount AS INTEGER NO-UNDO.

{&OUT} '<HTML>~n'.
{&OUT} '<HEAD>~n'.
{&OUT} '<!-- E4GL Disabled: META HTTP-EQUIV="Content-Type" CONTENT="text/html~; CHARSET=iso-8859-1" -->~n'.
{&OUT} '<TITLE>Sports Customer Report</TITLE>~n'.
{&OUT} '</HEAD>~n'.

{&OUT} '<BODY BGCOLOR="#FFFFFF">~n'.
{&OUT} '<H1>Customer List</H1>~n'.
{&OUT} '<TABLE BORDER="1">~n'.
{&OUT} '  <TR>~n'.
{&OUT} '    <TH>Customer ID</TH>~n'.
{&OUT} '    <TH>Customer Name</TH>~n'.
{&OUT} '    <TH>Phone Number</TH>~n'.
{&OUT} '  </TR>~n'.
    FOR EACH Customer:
        iCount = iCOunt + 1.
{&OUT} '  <TR>~n'.
{&OUT} '    <TD ALIGN="left">'  CustNum  '</TD>~n'.
{&OUT} '    <TD ALIGN="left">'  NAME     '</TD>~n'.
{&OUT} '    <TD ALIGN="left">'  Phone    '</TD>~n'.
{&OUT} '  </TR>~n'.
       IF iCount >= 100 THEN LEAVE.
    END.
{&OUT} '</TABLE>~n'.
{&OUT} '</BODY>~n'.
{&OUT} '</HTML>~n'.

PROCEDURE local-e4gl-options :
  DEFINE OUTPUT PARAMETER p_version AS DECIMAL NO-UNDO
    INITIAL 2.0.
  DEFINE OUTPUT PARAMETER p_options AS CHARACTER NO-UNDO
    INITIAL "web-object":U.
  DEFINE OUTPUT PARAMETER p_content-type AS CHARACTER NO-UNDO
    INITIAL "text/html~; CHARSET=iso-8859-1":U.
END PROCEDURE.

